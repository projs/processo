# Documentação API - Processo Classificação
(back para acesso a api de classificação e extração de entidades (IA))

### Swagger 2
- http://localhost:9090/api/swagger-ui.html

### RAML
- http://localhost:9090/api/v2/api-docs

### acesso pelo frontend
- http://localhost:9090/api/site/index.html

O usuário deve ser previamente cadastrado, conforme instruções abaixo.
- login: jose
- senha: 123

### Processo de instalação:
- docker run --name postgres -e POSTGRES_USER=processo -e POSTGRES_PASSWORD=processo -e POSTGRES_DB=processo -p 5432:5432 -d postgres

- git clone https://github.com/deviantony/docker-elk.git
- cd /docker-elk
- docker-compose up -d

- Execução:
    java -server -jar processo-1.0.0.jar &

#### Login
**[POST]** http://localhost:9090/api/login 
Efetua autenticação, é retornado um JWT no HEADER Authorization.
Exemplo:

```
{
	"login": "jose",
	"password": "123"
}
```

#### Usuário

**[POST]** https://localhost:9090/api/v1/users - Cadastra um novo usuário.
Exemplo:
```
{
	"login": "jose",
	"password": "123"	
}
```

### Considerações aos critérios de Entrega:

##### Propor um desenho arquitetural com base em UML (Pacote, Componentes e Implantação) para representar a solução.
- Arquivo PDF a parte no repositório.

##### Propor uma solução performática para o tráfego de arquivos intercambiáveis na API.
    ##### Proposta de melhorias na versão da API:
    - suportar modelo assíncrono e IO não bloqueante, baseado em Netty
    - suportar upload/download de arquivos via streaming e chunked upload / download
    - feito: api usa gzip para tráfego de dados
    - feito: api usa cache para aumentar throughput, via redis

##### Propor um controle de segurança (​Token)​ na API desenvolvida (Justificar a escolha do controle de segurança).
- Adotado JWT e o uso do spring-security, chave de BCrypt personalizável, via application.properties
 
##### Documentar a API desenvolvida com base no​ Swagger (RAML)
- http://localhost:9090/api/v2/api-docs (RAML)
- http://localhost:9090/api/swagger-ui.html: 

        Endpoints
        Os endpoint /login e /users, não exigem autenticacão, considerando o método POST, são publicos.
        Os demais, precisam que seja incluído o HEADER Authorization para que possam ser executados. O valor desse HEADER é retornado através da chamada ao endpoint /login.

        O atributo login do Usuário, deve ser único e é validado nas requisições.

        O swagger está suportando simulação além de apenas documentação.
        Para as operações que exigem authorização, deve ser informado o token no botão Authorize, na página do swagger

##### Propor um controle de ​log​ na aplicação. (Justificar a escolha do controle de ​log)​
- Foi adotado para produção, o EK (Elasticsaerch, Kibana), pois a API pode ser escalada para rodar em container e o mesmo iniciar/matar a aplicação conforme regras de autoscaling.
- Implementado log assincrono, com buffer e escrita paralela por meio do endpoint _bulk do ElasticSearch
- Index a ser considerado no kibana: logs-* 
- Log configurável através do arquivo logback.xml
- kibana: http://localhost:5601
- Adotado json, para permitir não quebrar linha em logs separados(como o padrão do logstach, que dificulta o controle) e suporte a MDC, com conjunto de chaves configuráveis, podendo ser mensuradas no kibana, conforme interesse da aplicação/devops;

##### Adotar um ​design pattern coerente que consiga intermediar Ata e Publicação bem como a possibilidade de classes futuras, de forma que interfaces incompatíveis trabalhem juntas
- Adotado os patters: Adapter e Plugin, via service loaders (java: IProcesso, ProcessoAdapter, ... )
- Plugins podem ser incorparados em .jars externos, sem necessidade de re-deploy

##### tela de configuração deverá persistir as URLs dos ​end-points de Ata e Publicação em um banco de dados utilizando uma framework de ORM. (Justificar a escolha da framework e do banco de dados, podendo ser ​Relational, Non-Relational ou In-Memory)​ .
- Adotado: JPA e Spring Data
- Adotado Postgres: devido ao campos já serem especificados, não havendo necessidade de NoSQL

## Profiles
O projeto está configurado para usar o recurso de profiles do Spring Framework, assim basta chamar via linha de comando o perfil que se deseja que seja executado. 
java -Dspring.profiles.active=prod -jar processo-1.0.0.jar &

Também é possível alterar o ambiente, mudando as configurações no arquivo:

- application.properties


