# Instalações necessárias: 
- docker run --name postgres -e POSTGRES_USER=processo -e POSTGRES_PASSWORD=processo -e POSTGRES_DB=processo -p 5432:5432 -d postgres
- docker run --name adminer -p 8080:8080 --link postgres:postgres -d adminer



- docker run --name mongodb -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=admin -d mongo:4
- docker exec -it mongodb mongo --host localhost -u admin -p admin --authenticationDatabase admin --eval "db.getSiblingDB('processo').createUser({user: 'processo', pwd: 'processo', roles: [{role: 'readWrite', db: 'processo'}]})"

## ELK:
- git clone https://github.com/deviantony/docker-elk.git
- cd /docker-elk
- docker-compose up -d

### Ver indices no elasticsearch
http://localhost:9200/_cat/indices?v

### Execução
java -server -jar processo-1.0.0.jar

