;
(function() {
	'use strict';

	var routes = function($routeProvider) {
		$routeProvider.when('/', {
			controller : 'HomeCtrl',
			templateUrl : 'app/components/home/homeView.html'
		}).when('/personalizar', {
			controller : 'PedidoCtrl',
			templateUrl : 'app/components/pedido/pedidoView.html'
		});
                
	};

	angular.module('lancheApp.routes').config([ '$routeProvider', routes]);
})();