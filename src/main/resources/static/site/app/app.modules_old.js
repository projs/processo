;
(function() {
	'use strict';

	// Declarando o modulo pizzariaApp e suas dependências
	angular.module('lancheApp', [ 'lancheApp.routes', 'lancheApp.controllers']);

	// Declarando os demais modulos: services, controllers, routes ...
	angular.module('lancheApp.services', []);
	angular.module('lancheApp.directives', []);
	angular.module('lancheApp.controllers', [ 'lancheApp.services' ]);
	angular.module('lancheApp.routes', [ 'ngRoute']);
})();
