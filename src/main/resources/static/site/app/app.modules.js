;
(function() {
	'use strict';

	// Declarando o modulo apiApp e suas dependências
	angular.module('apiApp', [ 'apiApp.routes', 'apiApp.controllers']);

	// Declarando os demais modulos: services, controllers, routes ...
	angular.module('apiApp.services', []);
	angular.module('apiApp.directives', []);
	angular.module('apiApp.controllers', [ 'apiApp.services' ]);
	angular.module('apiApp.routes', [ 'ngRoute']);
})();
