;
(function() {
	'use strict';

	var routes = function($routeProvider) {
		$routeProvider.when('/', {
			controller : 'HomeCtrl',
			templateUrl : 'app/components/home/homeView.html'
		});
                
	};

	
	angular.module('apiApp.routes').config([ '$routeProvider', routes]);
})();