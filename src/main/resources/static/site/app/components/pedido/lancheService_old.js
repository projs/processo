;
(function() {
	'use strick';

	// Service "pizzaService"
	var lancheService = function($http) {
		this.findAll = function(callback) {
			console.log('busca lanches ...');
			var request = {
				method : 'GET',
				url : '/api/v1/lanches',
				headers : {
					'Accept' : 'application/json'
				}
			};

			$http(request).success(callback).error(function(data) {
				alert('erro');
			});
		};

		this.saveLanche = function(lanche, callback) {
			console.log('faz pedido cardapio: ' + lanche);
			var request = {
				method : 'POST',
				url : '/api/v1/pedidos',
				headers : {
					'Accept' : 'application/json',
					'Content-Type' : 'application/json'
				},
				data : {"idLanche": lanche.id }
			};
                        
			$http(request).success(callback).error(function(data) {
				alert('erro: no update');
			});
		};

		this.findIngredientes = function(callback) {
			console.log('busca ingredientes ... ');
			var request = {
				method : 'GET',
				url : '/api/v1/ingredientes',
				headers : {
					'Accept' : 'application/json'
				}
			};

			$http(request).success(callback).error(function(data) {
				alert('erro');
			});
		};
                
                this.enviarPedido = function(pedido, callback) {
			console.log('faz pedido personalizado: ' + pedido);
                      
			var request = {
				method : 'POST',
				url : '/api/v1/pedidos',
				headers : {
					'Accept' : 'application/json',
					'Content-Type' : 'application/json'
				},
				data : pedido
			};
                        
			$http(request).success(callback).error(function(data) {
				alert('erro: no update');
			});
		};
	};

	/* Registrando o novo service "pizzaService" */
	angular.module('lancheService.services')
			.service('lancheService', lancheService);
})();