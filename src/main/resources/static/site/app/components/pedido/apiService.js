;
(function() {
	'use strick';

	var apiService = function($http) {
		this.findAll = function(callback) {
			console.log('busca lanches ...');
			var request = {
				method : 'GET',
				url : '/api/v1/lanches',
				headers : {
					'Accept' : 'application/json'
				}
			};

			$http(request).success(callback).error(function(data) {
				alert('erro');
			});
		};

		this.findConfig = function(token, callback) {
			console.log('busca de configuracao ...');
			var request = {
				method : 'GET',
				url : '/api/v1/configuracoes',
				headers : {
					'Accept' : 'application/json',
					"Authorization" : token
				}
			};

			$http(request).success(callback).error(function(data) {
				alert('erro');
			});

		}

		this.login = function(usuario, callback) {
			var request = {
				method : 'POST',
				url : '/api/login',
				headers : {
					'Accept' : 'application/json',
					'Content-Type' : 'application/json'
				},
				data : {"login": usuario.login.trim(), "password" : usuario.password.trim() }
			}


			$http(request).then(function successCallback(data, status, header, confi) {
				console.log(data.headers('Authorization'));
				callback(data.headers('Authorization'));
			}, function errorCallback(response) {
				console.log("erro");
			});
		};

		this.gravar = function(configuracao, token, callback) {
			console.log('gravando configuracao : ');

			var request = {
				method : 'PUT',
				url : '/api/v1/configuracoes',
				headers : {
					'Accept' : 'application/json',
					'Content-Type' : 'application/json',
					"Authorization" : token
				},
				data : {"chave": "ata", "url": configuracao.ata }
			};

			var status = false;
			$http(request).then(function successCallback(data, status, header, confi) {
				status = true;
			}, function errorCallback(response) {
				console.log("Erro ao salver url de ata.");
			});

			request = {
				method : 'PUT',
				url : '/api/v1/configuracoes',
				headers : {
					'Accept' : 'application/json',
					'Content-Type' : 'application/json',
					"Authorization" : token
				},
				data : {"chave": "publicacao", "url": configuracao.publicacao }
			};

			$http(request).then(function successCallback(data, status, header, confi) {
				callback(data);
			}, function errorCallback(response) {
				console.log("Erro ao salver url de publicacao.");
			});


		};

		this.classificar = function(classificacao, token, callback) {
			console.log('faz classificacao de : ' + classificacao.tipo);

				var request = {
					method : 'POST',
					url : '/api/v1/classificacoes',
					headers : {
						'Accept' : 'application/json',
						'Content-Type' : 'application/json',
						"Authorization" : token
					},
					data : {"tipo": classificacao.tipo, "texto": classificacao.texto, "confiancaDesejada" : classificacao.confiancaDesejada }
				};

			$http(request).success(callback).error(function(data) {
				alert('erro');
			});


		};

	};

	/* Registrando o novo service "apiService" */
	angular.module('apiApp.services')
			.service('apiService', apiService);
})();