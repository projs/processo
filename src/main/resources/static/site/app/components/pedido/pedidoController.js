;
(function() {
	'use strict';

	// Controller PedidoCtrl que depende de "lancheService"
	var pedidoCtrl = function($scope, lancheService) {

		var loadIngredientes = function() {
			lancheService.findIngredientes(function(data) {
                            
                            $scope.pedido = {
                                   promocao: "", ingredientes: []
                            };
                            for (var i = 0; i < data.length; i++) {
                                console.log(data[i]);
                                $scope.pedido.ingredientes.push({id: data[i].id, nome: data[i].nome, valor: data[i].valor, qtde: 0});
                            }
				//$scope.ingredientes = data;
                            console.log($scope.pedido);    
                                
			});
		};
                
                $scope.enviarPedido = function(pedido) {
                    console.log("enviando pedido pers: " + pedido)
                    
                    var pedidoPersonalizado = {
                                   promocao: pedido.promocao, ingredientes: []
                    };
                    for (var i = 0; i < pedido.ingredientes.length; i++) {
                        pedidoPersonalizado.ingredientes.push({ingredienteId: pedido.ingredientes[i].id, qtde: pedido.ingredientes[i].qtde});
                    }
                    
                    lancheService.enviarPedido(pedidoPersonalizado, function(data) {                            
                            $scope.pedido = pedidoPersonalizado;
                            $scope.retorno = data;
                            loadIngredientes();
                                               
                    });
	    };
            
            $scope.limpar = function() {
                $scope.retorno = null;
                $scope.pedido = null;
            }

		// Carregando os lanches
		//loadIngredientes();
	};

	/* Registrando o novo controller pedidoCtrl */
	angular.module('lancheApp.controllers')
			.controller('PedidoCtrl', pedidoCtrl);
})();