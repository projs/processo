;
(function() {
	'use strict';
	
	var homeCtrl = function($scope, apiService) {

	        var zera = function() {
                $scope.autenticacao = null;
            }
		
            var loadConfigs = function() {
                apiService.findConfig($scope.autenticacao,function(data) {
                    if (data.length > 0) {
                        var chave0 = data[0].chave;
                        var chave1 = data[1].chave;
                        if (chave0 === "publicacao")
                            $scope.configuracao.publicacao = data[0].url;
                        else
                            $scope.configuracao.ata = data[0].url;

                        if (chave1 === "ata")
                            $scope.configuracao.ata = data[1].url;
                        else
                            $scope.configuracao.publicacao = data[1].url;
                    }
		        });
            };
                
            $scope.openModal = function() {
                $scope.classificacao =  {};
                $scope.configuracao = {};
                $scope.mensagem = null;
                apiService.findConfig($scope.autenticacao, function (data) {
                    //console.warn(data);
                    if (data.length > 0) {
                        var chave0 = data[0].chave;
                        var chave1 = data[1].chave;
                        if (chave0 === "publicacao")
                            $scope.configuracao.publicacao = data[0].url;
                        else
                            $scope.configuracao.ata = data[0].url;

                        if (chave1 === "ata")
                            $scope.configuracao.ata = data[1].url;
                        else
                            $scope.configuracao.publicacao = data[1].url;
                    }

                    //console.warn(data[0].chave + " - " + data[1].chave);
                    //$scope.configuracao.ata = data.url;

                })
                $('#configModal').modal();
            };

            $scope.gravar = function(configuracao) {
                $scope.classificacao =  {};
                $scope.configuracao = {};
                apiService.gravar(configuracao, $scope.autenticacao, function (data) {
                    $scope.mensagem = "Gravado com sucesso!";
                    loadConfigs();

                });
            };

            $scope.login = function(usuario) {
                apiService.login(usuario, function(data) {
                    $scope.autenticacao = data;
                    $scope.status = "Autenticado: " + $scope.autenticacao;
                    console.warn($scope.autenticacao);

                })
            }

            $scope.publicacao = function() {
                var pub = {
                    tipo: "publicacao", texto: "Conteúdo da publicação", confiancaDesejada: 65
                };
                $scope.classificacao = pub;
            }

            $scope.ata = function() {
                var ata = {
                    tipo: "ata", texto: "http://download.gracco.finchsolucoes.com.br/?Parametro=5CzZAvgSP8rw6PiJSyA7AHIaibYJtCjBxUxTy9PhMZw49NLVl3eAC722jHIic1tf9OABgVJcZHxlYbzBH0fwjyO0jjhTv4Gl", confiancaDesejada: 65
                };
                $scope.classificacao = ata;
            }

        $scope.ata2 = function() {
            var ata = {
                tipo: "ata", texto: "http://download.gracco.finchsolucoes.com.br/?Parametro=5CzZAvgSP8rw6PiJSyA7AHIaibYJtCjBxUxTy9PhMZw49NLVl3eAC722jHIic1tf9OABgVJcZHxlYbzBH0fwjyO0jjhTv4Gl;http://download.gracco.finchsolucoes.com.br/?Parametro=5CzZAvgSP8rw6PiJSyA7AHIaibYJtCjBxUxTy9PhMZw49NLVl3eAC722jHIic1tf9OABgVJcZHxlYbzBH0fwjyO0jjhTv4Gl", confiancaDesejada: 65
            };
            $scope.classificacao = ata;
        }
            
            $scope.classificar = function(classificacao) {
                $scope.retorno = "Executando ...";

                apiService.classificar(classificacao, $scope.autenticacao ,function(data) {
                    console.warn(data)
                    if (classificacao.tipo === "ata") {
                        //console.warn(data.Ata)
                        if (data.Ata.Resultado === "")
                            $scope.retorno = data.Ata.Mensagem;
                        else
                         $scope.retorno = data.Ata.Resultado + " - " + data.Ata.Confianca + "% de confiança da IA";
                        var conf = parseFloat(data.Ata.Confianca);
                        if (parseFloat(classificacao.confiancaDesejada) > conf)
                            $scope.retorno += " - Enviar para tratamento manual";
                    } else {
                        //console.warn(data.Publicacao)
                        if (data.Publicacao.Resultado === "")
                            $scope.retorno = data.Publicacao.Mensagem;
                        else
                            $scope.retorno = data.Publicacao.Resultado + " - " + data.Publicacao.Confianca + "% de confiança da IA";
                        parseFloat(data.Publicacao.Confianca)
                        if (parseFloat(classificacao.confiancaDesejada) > conf)
                            $scope.retorno += " - Enviar para tratamento manual";
                    }
                });
	        };
            
            $scope.clear = function() {
			    $scope.classificacao = {};
                $scope.configuracao = {};
                $scope.mensagem = null;

	        };

	};
        
        

	/* Registrando o novo controller HomeCtrl */
	angular.module('apiApp.controllers').controller('HomeCtrl', homeCtrl);
})();