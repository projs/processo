package br.com.finch.api.processo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RequestDTO {

    @JsonProperty("Publicacao")
    private Publicacao publicacao;
}
