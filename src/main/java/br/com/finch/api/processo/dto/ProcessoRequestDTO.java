package br.com.finch.api.processo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@EqualsAndHashCode
public class ProcessoRequestDTO implements Serializable {

    private String tipo;
    private String texto;
    private Double confiancaDesejada;
}
