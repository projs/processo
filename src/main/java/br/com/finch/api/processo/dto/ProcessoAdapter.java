package br.com.finch.api.processo.dto;

public class ProcessoAdapter implements IProcesso {

    private IProcesso iProcesso;

    public ProcessoAdapter(IProcesso processo) {
        this.iProcesso = processo;
    }

    @Override
    public String processa(String url, ProcessoRequestDTO processoDTO) {
        return iProcesso.processa(url, processoDTO);
    }

    @Override
    public Object get() {
        return iProcesso.get();
    }

    @Override
    public String getTipo() {
        return iProcesso.getTipo();
    }
}
