package br.com.finch.api.processo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RequestAtaDTO {

    @JsonProperty("Ata")
    private Ata ata;
}
