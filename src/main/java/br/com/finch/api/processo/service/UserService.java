package br.com.finch.api.processo.service;

import br.com.finch.api.processo.domain.User;
import br.com.finch.api.processo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public User findById(Integer id) throws EntityNotFoundException {
        return userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("User not found"));
    }

    public List<User> findAllByIds(List<Integer> ids) {
        return userRepository.findAllByIds(ids);
    }

    public User save(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);

    }

    public User update(User user, Integer id) throws EntityNotFoundException {
        final User userManaged = findById(id);
        userManaged.setLogin(user.getLogin());
        if (!userManaged.getPassword().equals(user.getPassword())) {
           userManaged.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        return save(userManaged);
    }

}
