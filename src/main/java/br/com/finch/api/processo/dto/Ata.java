package br.com.finch.api.processo.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Ata implements IProcesso {

    @JsonProperty("IdSolicitacao")
    private Long idSolicitacao;
    @JsonProperty("Arquivos")
    private List<Arquivo> arquivos;

    public Ata(Long idSolicitacao, List<Arquivo> arquivos) {
        this.idSolicitacao = idSolicitacao;
        this.arquivos = arquivos;
    }

    public Ata() {
        super();
    }

    @Override
    public String processa(String url, ProcessoRequestDTO processoDTO) {
        RestTemplate restTemplate = new RestTemplate();
        Random random = new Random();
        this.idSolicitacao = random.nextLong();
        String[] arqs = processoDTO.getTexto().split(";");
        this.arquivos = new ArrayList<>();
        for (String nome : arqs) {
            Arquivo arq = new Arquivo();
            arq.setArquivo(nome);
            this.arquivos.add(arq);
        }
        RequestAtaDTO requestAtaDTO = new RequestAtaDTO();
        requestAtaDTO.setAta(this);

        ObjectMapper m = new ObjectMapper();
        String req = "";
        try {
            req = m.writeValueAsString(requestAtaDTO);
            System.out.println("req => " + req);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        String response = restTemplate.postForObject(url, requestAtaDTO, String.class);
        return response;
    }

    @Override
    public Object get() {
        return null;
    }

    @Override
    @JsonIgnore
    public String getTipo() {
        return "ata";
    }

}
