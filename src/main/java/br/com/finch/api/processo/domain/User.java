package br.com.finch.api.processo.domain;

import br.com.finch.api.processo.service.validation.UserValidate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Entity
@Table(name="users")
@Data
@UserValidate
@ApiModel(description = "Usuario")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Id do usuário")
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	@Nullable
	private Integer id;

	@ApiModelProperty(notes = "login do usuário")
	@Column(unique=true)
	@NotEmpty(message="login - required field")
	private String login;

	@ApiModelProperty(notes = "password do usuário")
	@NotEmpty(message="password - required field")
	private String password;

        
	public User() {
		super();
	}

	public User(Integer id, String login, String password) {
		this.id = id;
		this.login = login;
		this.password = password;
	}
	
}
