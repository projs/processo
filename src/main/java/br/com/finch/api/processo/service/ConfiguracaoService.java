package br.com.finch.api.processo.service;

import br.com.finch.api.processo.domain.Configuracao;
import br.com.finch.api.processo.repository.ConfiguracaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class ConfiguracaoService {

    @Autowired
    private ConfiguracaoRepository configuracaoRepository;

    public Configuracao findById(Integer id) throws EntityNotFoundException {
        return configuracaoRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Configuracao not found"));
    }

    public List<Configuracao> findAll() {
        return configuracaoRepository.findAll();
    }

    public Configuracao findByChave(String chave) throws EntityNotFoundException {
        return configuracaoRepository.findByChave(chave)
                .orElseThrow(() -> new EntityNotFoundException("Configuracao not found"));
    }

    public Configuracao save(Configuracao configuracao) {
        return configuracaoRepository.save(configuracao);
    }

    public Configuracao update(Configuracao configuracao) throws EntityNotFoundException {
        final Configuracao configuracaoManaged = this.findByChave(configuracao.getChave());
        configuracaoManaged.setUrl(configuracao.getUrl().trim());
        return save(configuracaoManaged);
    }
}
