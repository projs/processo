package br.com.finch.api.processo.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class Numero implements Serializable {

    private String numero;
}
