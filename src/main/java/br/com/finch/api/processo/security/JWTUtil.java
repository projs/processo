package br.com.finch.api.processo.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Value;

import java.util.Date;
import java.util.Objects;

@Component
public class JWTUtil {

    @Value("${security.key}")
    private String securityKey;

    @Value("${security.expiration}")
    private Long timeExpiration;

    public String generateToken(String login) {
        return Jwts.builder()
                .setSubject(login)
                .setExpiration(new Date(System.currentTimeMillis() + timeExpiration))
                .signWith(SignatureAlgorithm.HS512, securityKey.getBytes())
                .compact();
    }

    public boolean isValidToken(String token) {
        final Claims claims = getClaims(token);
        if (Objects.isNull(claims)) {
            return false;
        }

        final String username = claims.getSubject();
        final Date expirationDate = claims.getExpiration();
        final Date currentDate = new Date(System.currentTimeMillis());

        return Objects.nonNull(username) && Objects.nonNull(expirationDate) && currentDate.before(expirationDate);
    }

    public String getUsername(String token) {
        final Claims claims = getClaims(token);
        if (Objects.isNull(claims)) {
            return null;
        }

        return claims.getSubject();
    }

    private Claims getClaims(String token) {
        try {
            return Jwts.parser().setSigningKey(securityKey.getBytes()).parseClaimsJws(token).getBody();
        } catch (Exception e) {
            return null;
        }
    }

}