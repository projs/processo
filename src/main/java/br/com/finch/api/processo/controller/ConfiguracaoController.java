package br.com.finch.api.processo.controller;

import br.com.finch.api.processo.domain.Configuracao;
import br.com.finch.api.processo.dto.ConfiguracaoDTO;
import br.com.finch.api.processo.service.ConfiguracaoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/configuracoes")
@Slf4j
@Api(value="/v1/configuracoes", description="APIs de gerenciamento de configuração de URL relacionadas à APIs de classificação")
public class ConfiguracaoController {

    @Autowired
    private ConfiguracaoService configuracaoService;

    @ApiOperation(value = "Obtém todas as configurações cadastradas")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Configuraçoes retornadas com sucesso")
    })
    @GetMapping
    public ResponseEntity<List<Configuracao>> findAll() {
        return ResponseEntity.ok(configuracaoService.findAll());
    }

    @ApiOperation(value = "Insere uma configuração de URL de classificacao conforme sua chave")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Configuração inserida com sucesso"),
            @ApiResponse(code = 401, message = "Acesso não autorizado"),
            @ApiResponse(code = 403, message = "Acesso ao recurso proibido"),
            @ApiResponse(code = 500, message = "Erro ao processar requisição")

    })
    @PostMapping
    public ResponseEntity<Void> insert(@RequestBody ConfiguracaoDTO configuracao) {
        configuracaoService.save(new Configuracao(configuracao.getChave(), configuracao.getUrl().trim()));
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "Atualiza a configuração de uma URL de classificacao conforme sua chave")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Configuração atualizada com sucesso"),
            @ApiResponse(code = 401, message = "Acesso não autorizado"),
            @ApiResponse(code = 403, message = "Acesso ao recurso proibido"),
            @ApiResponse(code = 500, message = "Erro ao processar requisição")

    })
    @PutMapping
    public ResponseEntity<Void> update(@Valid @RequestBody ConfiguracaoDTO configuracao) throws EntityNotFoundException {
        try {
            Configuracao configuracaoManaged = configuracaoService.findByChave(configuracao.getChave());
            configuracaoManaged.setUrl(configuracao.getUrl().trim());
            configuracaoService.update(configuracaoManaged);
        } catch (EntityNotFoundException ex) {
            configuracaoService.save(new Configuracao(configuracao.getChave(), configuracao.getUrl().trim()));
        }
        return ResponseEntity.ok().build();
    }
}
