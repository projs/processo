package br.com.finch.api.processo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class Arquivo implements Serializable {

    @JsonProperty("Arquivo")
    private String arquivo;
}
