package br.com.finch.api.processo.dto;

public interface IProcesso {

    String processa(String url, ProcessoRequestDTO processoDTO);

    Object get();

    String getTipo();
}
