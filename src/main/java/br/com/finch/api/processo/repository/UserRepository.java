package br.com.finch.api.processo.repository;

import br.com.finch.api.processo.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface UserRepository extends CrudRepository<User, Integer> {
        
	User findByLogin(String login);
        
	@Query(value = "select u from User u where u.id in (:ids)")
    List<User> findAllByIds(@Param("ids") List<Integer> ids);
}
