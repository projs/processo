package br.com.finch.api.processo.service;


import br.com.finch.api.processo.domain.User;
import br.com.finch.api.processo.repository.UserRepository;
import br.com.finch.api.processo.security.UserAuthority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		final User user = userRepository.findByLogin(login);
		if (Objects.isNull(user)) {
			throw new UsernameNotFoundException(login);
		}

		return new UserAuthority(user.getId(), user.getLogin(), user.getPassword());
	}

}
