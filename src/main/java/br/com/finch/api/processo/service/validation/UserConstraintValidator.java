package br.com.finch.api.processo.service.validation;

import br.com.finch.api.processo.domain.User;
import br.com.finch.api.processo.error.AttributeError;
import br.com.finch.api.processo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class UserConstraintValidator implements ConstraintValidator<UserValidate, User> {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean isValid(User user, ConstraintValidatorContext context) {
        if (Objects.isNull(request)) {
            return true;
        }

        final List<AttributeError> messages = new ArrayList<>();

        if (getRequestedMethod("PUT")) {
            final Map<String, String> map = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
            final Integer uriId = Integer.valueOf(map.get("id"));

            User userDb = userRepository.findByLogin(user.getLogin());
            if (Objects.nonNull(userDb) && userDb.getId().compareTo(uriId) != 0) {
                messages.add(new AttributeError("login", "Login already exists"));
            }
        } else {
            User userManaged = userRepository.findByLogin(user.getLogin());
            if (Objects.nonNull(userManaged)) {
                messages.add(new AttributeError("login", "Login already exists."));
            }
        }

        for (AttributeError fieldMessage : messages) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(fieldMessage.getMessage())
                    .addPropertyNode(fieldMessage.getFieldName())
                    .addConstraintViolation();
        }

        return messages.isEmpty();
    }

    private boolean getRequestedMethod(String method) {
        return request.getMethod().equals(method);
    }
}
