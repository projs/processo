/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.finch.api.processo.config;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Date;
import java.util.List;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 *
 * @author jcalbertin
 * 
 * 
 */
@Configuration
@EnableSwagger2
@Slf4j
public class SwaggerConfig {

    /*
    @Bean
    public ApiListingScannerPlugin scan(){
        return new ApiListingScannerPlugin(){

            @Override
            public List<ApiDescription> apply(DocumentationContext documentationContext) {
                return new ArrayList<ApiDescription>(
                        Arrays.asList(
                                new ApiDescription(
                                        "/api/login",
                                        "Login",
                                        Arrays.asList(
                                                new OperationBuilder(new CachingOperationNameGenerator())
                                                        .authorizations(new ArrayList())
                                                        .codegenMethodNameStem("login")
                                                        .method(HttpMethod.POST)
                                                        .parameters(
                                                                Arrays.asList(
                                                                        new ParameterBuilder()
                                                                                .description("Login")
                                                                                .type(new TypeResolver().resolve(String.class))
                                                                                .name("login")
                                                                                .parameterType("body")
                                                                                .parameterAccess("access")
                                                                                .required(true)
                                                                                .modelRef(new ModelRef("string"))
                                                                                .build()
                                                                        ,new ParameterBuilder()
                                                                                .description("password")
                                                                                .type(new TypeResolver().resolve(String.class))
                                                                                .name("password")
                                                                                .parameterType("body")
                                                                                .parameterAccess("access")
                                                                                .required(true)
                                                                                .modelRef(new ModelRef("string"))
                                                                                .build()
                                                                )
                                                        )
                                                        .build()
                                        ),
                                        false
                                )
                        )
                );
            }

            @Override
            public boolean supports(DocumentationType documentationType) {
                return DocumentationType.SWAGGER_2.equals(documentationType);
            }
        };
    }
    */

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String DEFAULT_INCLUDE_PATTERN = "/v1.*";

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("API processos")
                .description("API de acesso às APIs de Classificação")
                .version("1.0")
                .contact(contact())
                .build();
    }

    private Contact contact() {
        return new Contact(
                "José Carlos",
                "",
                "josealbertin@finchsolucoes.com.br");
    }

    @Bean
    public Docket api() {
        log.info("Configurando Swagger ...");


        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                //.groupName("processo")

                .apiInfo(apiInfo())
                .pathMapping("/")
                //.apiInfo(ApiInfo.DEFAULT)
                .forCodeGeneration(true)
                .genericModelSubstitutes(ResponseEntity.class)
                .ignoredParameterTypes(Pageable.class)
                .ignoredParameterTypes(java.sql.Date.class)
                .directModelSubstitute(java.time.LocalDate.class, java.sql.Date.class)
                .directModelSubstitute(java.time.ZonedDateTime.class, Date.class)
                .directModelSubstitute(java.time.LocalDateTime.class, Date.class)
                .securityContexts(Lists.newArrayList(securityContext()))
                .securitySchemes(Lists.newArrayList(apiKey()))
                .useDefaultResponseMessages(false);

        docket = docket.select()
                .paths(regex(DEFAULT_INCLUDE_PATTERN))
                .build();

        log.warn("Fim do config do swagger 2");
        return docket;
    }


    private ApiKey apiKey() {
        return new ApiKey("JWT", AUTHORIZATION_HEADER, "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.regex(DEFAULT_INCLUDE_PATTERN))
                .build();
    }

    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope
                = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Lists.newArrayList(
                new SecurityReference("JWT", authorizationScopes));
    }

    //@Bean
    //public ApiListingScannerPlugin listingScanner() {
    //    return new SwaggerManualApiPlugin();
    //}


}
