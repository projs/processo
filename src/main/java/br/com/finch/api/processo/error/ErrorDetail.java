/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.finch.api.processo.error;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ErrorDetail implements Serializable {
    
    private String title;
    private int status;
    private String detail;
    private long timeStamp;
    private String endpoint;

    public ErrorDetail(String title, int status, String detail, long timeStamp, String endpoint) {
        this.title = title;
        this.status = status;
        this.detail = detail;
        this.timeStamp = timeStamp;
        this.endpoint = endpoint;
    }

    public ErrorDetail() {
    }
    
    

    
    
}
