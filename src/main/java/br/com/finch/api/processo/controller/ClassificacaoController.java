package br.com.finch.api.processo.controller;

import br.com.finch.api.processo.domain.Configuracao;
import br.com.finch.api.processo.dto.IProcesso;
import br.com.finch.api.processo.dto.ProcessoAdapter;
import br.com.finch.api.processo.dto.ProcessoRequestDTO;
import br.com.finch.api.processo.service.ConfiguracaoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;

@RestController
@RequestMapping("/v1/classificacoes")
@Slf4j
@Api("/v1/classificacoes")
public class ClassificacaoController {

    @Autowired
    private ConfiguracaoService configuracaoService;

    @ApiOperation(value = "Executa uma classificação baseada no tipo selecionado", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Usuário alterado com sucesso"),
            @ApiResponse(code = 500, message = "Não processado"),
            @ApiResponse(code = 403, message = "Acesso ao recurso proibido")

    })
    @PostMapping
    public ResponseEntity<String> processa(@Valid @RequestBody ProcessoRequestDTO processoDTO) {
        final Configuracao configuracao = configuracaoService.findByChave(processoDTO.getTipo());

        ServiceLoader<IProcesso> serviceLoaders = ServiceLoader.load(IProcesso.class);
        List<IProcesso> foos = new ArrayList<>();
        Iterator<IProcesso> p = serviceLoaders.iterator();
        while (p.hasNext()) {
            IProcesso t = p.next();
            foos.add(t);
        }
        //serviceLoaders.iterator().forEachRemaining(foos::add);
        String resposta = "";
        for (IProcesso f : foos) {

            if (f.getTipo().trim().equals(processoDTO.getTipo().trim())) {
                ProcessoAdapter adapter = new ProcessoAdapter(f);
                resposta = adapter.processa(configuracao.getUrl(), processoDTO);
            }
        }

        log.info("Resposta de requisicao: " + resposta.toString());
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<String>(resposta, httpHeaders, HttpStatus.OK);
    }
}
