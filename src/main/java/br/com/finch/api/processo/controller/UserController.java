package br.com.finch.api.processo.controller;

import br.com.finch.api.processo.domain.User;
import br.com.finch.api.processo.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/v1/users")
@Slf4j
//@Api("/v1/users")
@Api(value="Usuários", description="Serviços relacionados à usuários", basePath = "/v1/users")
public class UserController {

    @Autowired
    private UserService userService;


//    @GetMapping("{id}")
//    public ResponseEntity<User> find(@PathVariable("id") Integer id) throws EntityNotFoundException {
//        return ResponseEntity.ok( userService.findById(id) );
//    }

    @ApiOperation(value = "Insere um usuário")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Usuário inserido com sucesso."),
            @ApiResponse(code = 401, message = "Acesso não autorizado"),
            @ApiResponse(code = 403, message = "Acesso ao recurso proibido")

    })
    @PostMapping
    public ResponseEntity<Void> insert(@Valid @RequestBody User user) {
        userService.save(user);
        final URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(user.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @ApiOperation(value = "Insere um usuário")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Usuário alterado com sucesso"),
            @ApiResponse(code = 401, message = "Acesso não autorizado"),
            @ApiResponse(code = 403, message = "Acesso ao recurso proibido")

    })
    @PutMapping("{id}")
    public ResponseEntity<Void> update(@Valid @RequestBody User user, @PathVariable("id") Integer id) throws EntityNotFoundException {
        userService.update(user, id);
        return ResponseEntity.noContent().build();
    }

}
