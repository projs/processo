package br.com.finch.api.processo.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

public class Publicacao implements IProcesso {

    private static final long serialVersionUID = 1L;

    @JsonProperty("IdPublicacao")
    public Long idPublicacao;
    @JsonProperty("TextoPublicacao")
    private String textoPublicacao;
    private String uf;
    private String sistema;

    public Publicacao(Long idPublicacao, String textoPublicacao, String uf, String sistema) {
        this.idPublicacao = idPublicacao;
        this.textoPublicacao = textoPublicacao;
        this.uf = uf;
        this.sistema = sistema;
    }

    public Publicacao() {
        super();
    }

    @Override
    public String processa(String url, ProcessoRequestDTO processoDTO) {
        RestTemplate restTemplate = new RestTemplate();
        Random random = new Random();
        this.idPublicacao = random.nextLong();
        this.textoPublicacao = processoDTO.getTexto();
        this.uf = "";
        this.sistema = "";
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setPublicacao(this);

        /*
        ObjectMapper m = new ObjectMapper();
        String req = "";
        try {
            req = m.writeValueAsString(requestDTO);
            System.out.println("req => " + req);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        */

        String response = restTemplate.postForObject(url, requestDTO, String.class);
        return response;
    }

    @Override
    public Object get() {
        return this;
    }

    @Override
    @JsonIgnore
    public String getTipo() {
        return "publicacao";
    }


}
