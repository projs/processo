package br.com.finch.api.processo.error;

import java.util.ArrayList;
import java.util.List;

public class ValidationError extends ErrorDetail {
	private static final long serialVersionUID = 1L;

	private List<AttributeError> messages = new ArrayList<>();

	public ValidationError(String title, int status, String detail, long timeStamp, String endpoint) {
		super(title, status, detail, timeStamp, endpoint);
	}

	public List<AttributeError> getErros() {
		return messages;
	}

	public void addError(String attibute, String message) {
		messages.add(new AttributeError(attibute, message));
	}
	
}
