package br.com.finch.api.processo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(description = "ConfiguracaoDTO")
public class ConfiguracaoDTO implements Serializable {

    @ApiModelProperty(notes = "chave do tipo da URL")
    private String chave;

    @ApiModelProperty(notes = "URL da API de classificação")
    private String url;
}
