package br.com.finch.api.processo.repository;

import br.com.finch.api.processo.domain.Configuracao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Transactional
public interface ConfiguracaoRepository extends JpaRepository<Configuracao, Integer> {

    Optional<Configuracao> findByChave(String chave);

}
